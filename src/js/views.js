var views = {};
views.SubmarineView = Backbone.View.extend({
  tagName: "div",
  className: function () {
    return "col-md-3 " + this.model.attributes.state;
  },
  id: function () {
    return "submarine_" + this.model.attributes.id;
  },
  events: {
    "click .btn": "onHideButtonClick",
  },
  initialize: function () {
    this.listenTo(this.model, "change", this.render);
    this.template = _.template(
      "<h2><%= name %></h2>" +
        '<input type="hidden" id="subuuid" value="<%= id %>"></input>' +
        '<p><a class="btn btn-primary" href="#" role="button">Hide Submarine</a></p>'
    );
  },
  onHideButtonClick: function () {
    var submarine = this.model;
    submarine.set({ state: "disabled" });
    $("#submarine_" + submarine.id)
      .removeClass("show")
      .addClass("disabled");
    publishRequestToHideSubmarine({
      name: submarine.attributes.name,
      uuid: submarine.id,
    });
  },
  render: function () {
    this.$el.html(this.template(this.model.attributes));
    return this;
  },
});

// list view
views.SubmarineListView = Backbone.View.extend({
  el: "#submarines",
  initialize: function () {
    if (!("collection" in this)) {
      this.collection = new models.Submarine();
    }
    this.listenTo(this.collection, "add", this.onSubmarineAdded, this);
    this.listenTo(this.collection, "remove", this.onSubmarineRemoved, this);
    this._submarineViews = [];
  },
  onSubmarineAdded: function (submarine) {
    this._submarineViews[submarine.id] = new views.SubmarineView({
      model: submarine,
      container: this,
    });
    var submarineView = this._submarineViews[submarine.id];
    this.$el.append(submarineView.render().el);
  },
  onSubmarineRemoved: function (submarine) {
    submarine.set({ state: "hide" });
    $("#submarine_" + submarine.id).remove();
  },
  getSubmarineView: function (submarine) {
    var id = _.isObject(submarine) ? submarine.id : submarine;
    return id in this._submarineViews ? this._submarineViews[id] : null;
  },
  render: function () {
    $("#submarines").empty();
    var that = this;
    if (this.collection.length > 0) {
      this.collection.each(function (submarine) {
        that.$el.append(that.getSubmarineView(submarine).render().el);
      });
    }
    return this;
  },
});

views.SubmarineListView = new views.SubmarineListView({
  collection: models.submarineList,
});
